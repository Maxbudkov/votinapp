FROM python:3.11

RUN mkdir /voting_test
COPY . /voting_test/
WORKDIR /voting_test

RUN pip install -r requirements.txt

WORKDIR /voting_test/voting_test_task

CMD python manage.py migrate && python manage.py test votingapp.tests && python manage.py runserver 0.0.0.0:8000
