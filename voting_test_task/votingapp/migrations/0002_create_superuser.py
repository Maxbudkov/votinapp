"""
Create superuser migration for docker deployment
"""

from django.db import migrations

from voting_test_task.settings import DEFAULT_ADMIN_USER_PASSWORD
from ..models import ExtendedUser


def create_superuser(apps, schema_editor) -> None:
    """
    Superuser creation

    :param apps:
    :param schema_editor:
    :return: None
    """
    staff_user = ExtendedUser.objects.create(
        username='admin',
        email='admin@example.com',
        is_staff=True,
        is_superuser=True
    )
    staff_user.set_password(DEFAULT_ADMIN_USER_PASSWORD)
    staff_user.save()


class Migration(migrations.Migration):

    initial = False

    dependencies = [
        ('votingapp', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_superuser)
    ]
