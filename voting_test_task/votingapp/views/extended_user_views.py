from django.contrib.auth.hashers import make_password
from rest_framework import viewsets

from .helpers import IsAdminUserOrReadOnly
from ..models.extended_user import ExtendedUser
from ..serializers.extended_user_serializer import ExtendedUserSerializer


class ExtendedUserViewSet(viewsets.ModelViewSet):
    """
    ExtendedUser View Set representation
    """
    queryset = ExtendedUser.objects.all()
    serializer_class = ExtendedUserSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    def perform_create(self, serializer):
        password = serializer.validated_data.get('password')
        serializer.validated_data['password'] = make_password(password)
        serializer.save()
