import logging

import openpyxl
import os

from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail import EmailMessage
from django.db.models import Q, QuerySet, Count, Max, F
from django.http import HttpResponse
from django.utils import timezone
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from tempfile import TemporaryDirectory

from .helpers import IsAdminUserOrReadOnly
from ..models import ExtendedUser, Cast, Vote
from ..models.cast import get_query_to_vote_max_cast_by_target
from ..serializers.casts_serializer import CastExportAsFileSerializer
from ..serializers.vote_serializer import VoteSerializer, VoteSortQuerySerializer


def get_winners(vote_id: int) -> QuerySet:
    """
    Get winners

    :param vote_id: int
    :return: QuerySet
    """
    ordered_casts = get_casts_order_by_cast_count(vote_id)

    if ordered_casts.exists():
        ordered_casts = ordered_casts.filter(cast_count=Max(ordered_casts[0].get('cast_count')))
    return ordered_casts


class VoteViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vote View Set representation
    """
    queryset = Vote.objects.all()
    serializer_class = VoteSerializer
    permission_classes = [IsAdminUserOrReadOnly]

    def list(self, request, *args, **kwargs):
        current_time = timezone.now()
        serializer = VoteSortQuerySerializer(data=request.query_params)
        if serializer.is_valid():
            valid_data = serializer.validated_data
            votes_list = []

            if not any(valid_data.values()):
                return Response(VoteSerializer(Vote.objects.all(), many=True).data)

            if valid_data.get('is_started'):
                votes_list.extend(
                    Vote.objects.annotate(
                        max_cast_count=get_query_to_vote_max_cast_by_target()
                    )
                    .filter(
                        finished_at__gte=current_time,
                    ).filter(
                        Q(early_termination_vote_count=0)
                        | Q(finished_at__gte=current_time, max_cast_count__lt=F('early_termination_vote_count'))
                    )
                )

            if valid_data.get('is_finished'):
                votes_list.extend(
                    Vote.objects.filter(
                        finished_at__lte=current_time
                    )
                )

            if valid_data.get('is_early_finished'):
                votes_list.extend(
                    Vote.objects.annotate(
                        max_cast_count=get_query_to_vote_max_cast_by_target()
                    ).filter(
                        max_cast_count__gte=F('early_termination_vote_count')
                    )
                )

            return Response(VoteSerializer(votes_list, many=True).data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True)
    def winners(self, *args, **kwargs) -> Response:
        """
        Winners wrapper

        :param args: Any
        :param kwargs: Any
        :return: Response
        """
        return Response(
            get_winners(kwargs.get('pk')) if kwargs.get('pk') else [],
            status=status.HTTP_200_OK
        )


def get_casts_order_by_cast_count(vote_id: int) -> QuerySet:
    """
    Get casts sorted by cast_counts

    :param vote_id: int
    :return: QuerySet
    """
    if Vote.objects.annotate(
        max_cast_count=get_query_to_vote_max_cast_by_target()
    ).filter(
        (
                Q(id=vote_id)
                & (
                        Q(finished_at__lte=timezone.now())
                        | Q(max_cast_count__gte=F('early_termination_vote_count'))
                )
        )
    ).exists():
        return (
            Cast.objects.filter(vote_id=vote_id)
            .values('target')
            .annotate(cast_count=Count('target'))
            .order_by('-cast_count')
        )
    return Cast.objects.none()


@staff_member_required
def export_voting_results(request, vote_id: int) -> Response or HttpResponse:
    """
    Export voting results by vote_id

    :param request: Any
    :param vote_id: int
    :return: Response or HttpResponse
    """
    serializer = CastExportAsFileSerializer(data=request.GET)

    if serializer.is_valid():
        valid_data = serializer.validated_data

        vote = Vote.objects.get(id=vote_id)
        if not vote:
            return Response('Vote not found', status=status.HTTP_404_NOT_FOUND)

        cast_users = get_casts_order_by_cast_count(vote_id)

        if not cast_users:
            return Response('Casts not found', status=status.HTTP_404_NOT_FOUND)

        restored_targets = []

        for restored_target in ExtendedUser.objects.filter(
                id__in=[cast_user.get('target') for cast_user in cast_users]
        ).all():
            for cast_user in cast_users:
                if cast_user.get('target') == restored_target.id:
                    restored_targets.append(
                        {
                            'target': cast_user.get('target'),
                            'target_username': restored_target.username,
                            'cast_count': cast_user.get('cast_count')
                        }
                    )

        workbook = openpyxl.Workbook()
        worksheet = workbook.active

        worksheet.append(['Result by vote ' + vote.title])
        worksheet.append(['target_id', 'target_username', 'cast_count'])
        filename = f'voting_results_{vote.id}.xlsx'
        excel_file_type = 'application/ms-excel'

        for restored_target in restored_targets:
            worksheet.append(
                [
                    restored_target.get('target'),
                    restored_target.get('target_username'),
                    restored_target.get('cast_count')
                ]
            )

        with TemporaryDirectory() as temp_dir:
            temp_file_path = os.path.join(temp_dir, filename)
            workbook.save(temp_file_path)
            with open(temp_file_path, 'rb') as temp_file:
                file_content = temp_file.read()

            email_sender_list = list(
                filter(
                    None,
                    [
                        request.user.email,
                        vote.owner.email
                    ]
                )
            )

            if valid_data.get('send_email') and email_sender_list:
                mail = EmailMessage(
                    'Vote #' + str(vote.id),
                    'Check attached file',
                    settings.EMAIL_HOST_USER,
                    email_sender_list
                )
                mail.attach(
                    filename,
                    file_content,
                    excel_file_type
                )
                try:
                    mail.send()
                except Exception as exp:
                    logging.error('Email send exception: %s', exp)
                    return HttpResponse(f'Email send exception: {exp}', status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        response = HttpResponse(content=file_content, content_type=excel_file_type)
        response['Content-Disposition'] = f'attachment; filename="{filename}"'

        return response
