from rest_framework import viewsets, status
from rest_framework.response import Response

from ..models.cast import Cast
from ..serializers.casts_serializer import CastsSerializer


class CastViewSet(viewsets.ModelViewSet):
    """
    Cast View Set representation
    """
    queryset = Cast.objects.all()
    serializer_class = CastsSerializer

    def list(self, request, *args, **kwargs):
        return Response(status=status.HTTP_404_NOT_FOUND)
