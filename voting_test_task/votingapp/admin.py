from django.contrib import admin

from .models import Cast, ExtendedUser, Vote

admin.site.register(ExtendedUser)
admin.site.register(Vote)
admin.site.register(Cast)
