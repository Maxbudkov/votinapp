import json

from datetime import timedelta
from time import sleep

from django.db.models import F
from django.http import HttpRequest
from django.test import TestCase, Client
from django.utils import timezone

from ..models import Vote, ExtendedUser, Cast
from ..models.cast import get_query_to_vote_max_cast_by_target
from ..views.vote_views import VoteViewSet, get_winners


class VoteTestCase(TestCase):
    default_password = 'tested'

    def setUp(self):
        ExtendedUser.objects.create(
            username='test_user_1',
            password=self.default_password,
            age=20,
            bio='test bio 1'
        )
        ExtendedUser.objects.create(
            username='test_user_2',
            password=self.default_password,
            age=30,
            bio='test bio 2'
        )
        staff_user = ExtendedUser.objects.create(
            username='staff_user_1',
            age=30,
            bio='staff user',
            is_staff=True
        )
        staff_user.set_password(self.default_password)
        staff_user.save()

    def test_early_termination(self):
        """Vote early termination work"""
        current_vote = Vote.objects.create(
            title='early_termination',
            description='early termination vote',
            early_termination_vote_count=1,
            started_at=timezone.now(),
            finished_at=(timezone.now() + timedelta(days=1)),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())
        current_target = ExtendedUser.objects.get(username='test_user_1')
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=current_target
        )
        current_vote = Vote.objects.annotate(
            max_cast_count=get_query_to_vote_max_cast_by_target()
        ).filter(
            title='early_termination',
            max_cast_count__gte=F('early_termination_vote_count')
        ).first()
        self.assertEqual(current_vote.title, 'early_termination')

    def test_winners(self):
        """Get winners"""
        current_vote = Vote.objects.create(
            title='winners',
            description='winners vote',
            early_termination_vote_count=2,
            started_at=timezone.now(),
            finished_at=(timezone.now() + timedelta(days=1)),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())
        winner_target = ExtendedUser.objects.get(username='test_user_1')
        another_target = ExtendedUser.objects.get(username='test_user_2')
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=another_target
        )
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )
        winners = get_winners(current_vote.id)

        self.assertEqual(
            [
                winner.get('target')
                for winner in winners
                if winner.get('target')
            ],
            [winner_target.id]
        )

    def test_empty_winners(self):
        """Get winners, if not winners"""
        current_vote = Vote.objects.create(
            title='empty_winners',
            description='empty winners vote',
            early_termination_vote_count=2,
            started_at=timezone.now(),
            finished_at=(timezone.now() + timedelta(days=1)),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())
        winner_target = ExtendedUser.objects.get(username='test_user_1')
        another_target = ExtendedUser.objects.get(username='test_user_2')
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=another_target
        )
        winners = get_winners(current_vote.id)

        self.assertEqual(
            [
                winner.get('target')
                for winner in winners
                if winner.get('target')
            ],
            []
        )

    def test_double_winners(self):
        """Get winners, if double winners"""
        current_vote = Vote.objects.create(
            title='double_winners',
            description='double winners vote',
            started_at=timezone.now(),
            finished_at=timezone.now() + timedelta(seconds=1),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())
        winner_target = ExtendedUser.objects.get(username='test_user_1')
        another_winner_target = ExtendedUser.objects.get(username='test_user_2')
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=another_winner_target
        )
        sleep(1)
        winners = get_winners(current_vote.id)

        self.assertEqual(
            [
                winner.get('target')
                for winner in winners
                if winner.get('target')
            ],
            [
                winner_target.id,
                another_winner_target.id
            ]
        )

    def test_vote_is_started(self):
        """Get vote is started"""
        current_vote = Vote.objects.create(
            title='started_vote',
            description='started vote',
            started_at=timezone.now(),
            finished_at=timezone.now() + timedelta(days=1),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())

        request = HttpRequest()
        request.method = 'GET'
        request.GET['is_started'] = True
        started_votes = VoteViewSet.as_view({'get': 'list'})(request)
        started_votes.render()
        started_vote_ids = [vote.get('id') for vote in json.loads(started_votes.content.decode('utf-8'))]

        self.assertEqual(
            started_vote_ids,
            [current_vote.id]
        )

    def test_vote_is_finished_by_time(self):
        """Get vote is finished by time"""
        current_vote = Vote.objects.create(
            title='finished_vote',
            description='finished vote',
            started_at=timezone.now(),
            finished_at=timezone.now() - timedelta(days=1),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())

        request = HttpRequest()
        request.method = 'GET'
        request.GET['is_finished_by_time'] = True
        finished_votes = VoteViewSet.as_view({'get': 'list'})(request)
        finished_votes.render()
        finished_vote_ids = [vote.get('id') for vote in json.loads(finished_votes.content.decode('utf-8'))]

        self.assertEqual(
            finished_vote_ids,
            [current_vote.id]
        )

    def test_vote_is_finished_by_early_termination(self):
        """Get vote is finished by early termination"""
        current_vote = Vote.objects.create(
            title='finished_vote_by_early',
            description='finished vote by early',
            early_termination_vote_count=1,
            started_at=timezone.now(),
            finished_at=timezone.now() + timedelta(days=1),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())
        winner_target = ExtendedUser.objects.get(username='test_user_1')
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )

        request = HttpRequest()
        request.method = 'GET'
        request.GET['is_early_finished'] = True
        finished_votes = VoteViewSet.as_view({'get': 'list'})(request)
        finished_votes.render()
        finished_vote_ids = [vote.get('id') for vote in json.loads(finished_votes.content.decode('utf-8'))]

        self.assertEqual(
            finished_vote_ids,
            [current_vote.id]
        )

    def test_export_vote_result(self):
        """Test export vote result"""
        current_vote = Vote.objects.create(
            title='export_vote',
            description='export vote result',
            early_termination_vote_count=2,
            started_at=timezone.now(),
            finished_at=(timezone.now() + timedelta(days=1)),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())
        winner_target = ExtendedUser.objects.get(username='test_user_1')
        another_target = ExtendedUser.objects.get(username='test_user_2')
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=another_target
        )
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )
        current_vote = Vote.objects.get(title='export_vote')
        staff_user = ExtendedUser.objects.get(username='staff_user_1')

        client = Client()
        client.login(username=staff_user.username, password=self.default_password)

        response = client.get(f'/admin/export_voting_results/{current_vote.id}/')
        filename = f'voting_results_{current_vote.id}.xlsx'

        self.assertEquals(
            response.status_code,
            200
        )

        self.assertEquals(
            response.get('Content-Disposition'),
            f'attachment; filename="{filename}"'
        )

    def test_export_vote_result_is_hidden_for_non_staff_users(self):
        """Test export vote result for non staff users"""
        current_vote = Vote.objects.create(
            title='export_vote',
            description='export vote result',
            early_termination_vote_count=2,
            started_at=timezone.now(),
            finished_at=(timezone.now() + timedelta(days=1)),
            owner=ExtendedUser.objects.first()
        )
        current_vote.members.set(ExtendedUser.objects.all())
        winner_target = ExtendedUser.objects.get(username='test_user_1')
        another_target = ExtendedUser.objects.get(username='test_user_2')
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=another_target
        )
        Cast.objects.create(
            user=None,
            vote=current_vote,
            target=winner_target
        )
        current_vote = Vote.objects.get(title='export_vote')
        non_staff_user = ExtendedUser.objects.get(username='test_user_1')

        client = Client()
        client.login(username=non_staff_user.username, password=self.default_password)

        response = client.get(f'/admin/export_voting_results/{current_vote.id}/')

        # redirect on admin page, default marked as 302
        self.assertEquals(
            response.status_code,
            302
        )
