import uuid
from typing import Any

from django.contrib.auth.models import AbstractUser
from django.db import models


def user_directory_path(instance: Any, filename: str) -> str:
    """
    Folder structure as MEDIA_ROOT/user_<uuid4>/avatars/<filename>

    :param instance: ExtendedUser model instance
    :param filename: str
    :return: str
    """
    return f'user_{uuid.uuid4()}/avatars/{filename}'


class ExtendedUser(AbstractUser, models.Model):
    """
    User model representation
    """
    age = models.PositiveSmallIntegerField(blank=True, null=True)
    avatar = models.FileField(blank=True, null=True, upload_to=user_directory_path)
    middle_name = models.TextField(blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
