from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Subquery, Count, OuterRef
from django.utils import timezone

from .extended_user import ExtendedUser
from .vote import Vote


class Cast(models.Model):
    """
    Cast model representation
    """
    user = models.ForeignKey(ExtendedUser, on_delete=models.CASCADE, related_name='caster', blank=True, null=True)
    vote = models.ForeignKey(Vote, on_delete=models.CASCADE)
    target = models.ForeignKey(ExtendedUser, on_delete=models.CASCADE, related_name='cast_target')

    class Meta:
        """
        Additional info about model
        """
        constraints = [
            models.UniqueConstraint(
                fields=['user_id', 'vote_id'],
                name='cast_unique',
                condition=models.Q(user__isnull=False)
            )
        ]

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.__validate_target_membership()
            super().save(*args, **kwargs)

    def __validate_target_membership(self) -> None:
        """
        Валидация

        :return: None
        """
        with transaction.atomic():
            vote = Vote.objects.filter(id=self.vote.id).annotate(
                max_cast_count=get_query_to_vote_max_cast_by_target()
            ).first()
            if (
                    self.vote.finished_at <= timezone.now()
                    or (
                        vote.max_cast_count
                        and vote.early_termination_vote_count != 0
                        and vote.max_cast_count >= vote.early_termination_vote_count
                    )
            ):
                raise ValidationError('Vote or Target does not exist or Vote is finished now')

            if not self.vote.members.filter(id=self.target_id).exists():
                raise ValidationError('User is not a member of the Vote')


def get_query_to_vote_max_cast_by_target() -> Subquery:
    """
    Функция сортировки по max_cast_count

    :return: Subquery
    """
    return Subquery(
        Cast.objects
        .values('vote__id', 'target_id')
        .annotate(max_cast_count=Count('target'))
        .order_by('vote__id', '-max_cast_count')
        .filter(vote__id=OuterRef('id'))
        .values('max_cast_count')[:1]
    )
