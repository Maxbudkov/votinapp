from django.db import models
from .extended_user import ExtendedUser


class Vote(models.Model):
    """
    Vote model representation
    """
    id = models.BigAutoField(primary_key=True)
    title = models.TextField(db_index=True)
    description = models.TextField(blank=True, null=True)

    early_termination_vote_count = models.PositiveIntegerField(blank=True, null=True, default=0)

    started_at = models.DateTimeField()
    finished_at = models.DateTimeField()
    created_at = models.DateTimeField(blank=True, auto_now=True)

    owner = models.ForeignKey(ExtendedUser, on_delete=models.CASCADE)
    members = models.ManyToManyField(ExtendedUser, related_name='members')
