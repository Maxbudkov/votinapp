from rest_framework import serializers

from ..models import Cast


class CastsSerializer(serializers.ModelSerializer):
    """
    Cast serializer representation
    """
    class Meta:
        model = Cast
        fields = [
            'user',
            'vote',
            'target'
        ]


class CastExportAsFileSerializer(serializers.Serializer):
    send_email = serializers.BooleanField(default=False)
