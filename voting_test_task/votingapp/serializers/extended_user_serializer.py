from rest_framework import serializers

from ..models import ExtendedUser


class ExtendedUserSerializer(serializers.ModelSerializer):
    """
    ExtendedUser serializer representation
    """
    class Meta:
        model = ExtendedUser
        fields = [
            'id',
            'username',
            'avatar',
            'bio',
        ]
