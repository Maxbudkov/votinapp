from rest_framework import serializers

from ..models import Vote


class VoteSerializer(serializers.ModelSerializer):
    """
    Vote serializer representation
    """
    class Meta:
        model = Vote
        fields = [
            'id',
            'title',
            'description',
            'members'
        ]


class VoteSortQuerySerializer(serializers.Serializer):
    is_started = serializers.BooleanField(default=False)
    is_finished = serializers.BooleanField(default=False)
    is_early_finished = serializers.BooleanField(default=False)
