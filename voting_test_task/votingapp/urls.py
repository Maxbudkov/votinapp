from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views.cast_views import CastViewSet
from .views.extended_user_views import ExtendedUserViewSet
from .views.vote_views import VoteViewSet

router = DefaultRouter()
router.register('users', ExtendedUserViewSet)
router.register('votes', VoteViewSet)
router.register('casts', CastViewSet)

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
]

urlpatterns += router.urls
